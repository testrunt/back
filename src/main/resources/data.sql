--Crear tabla estudiantes
DROP TABLE IF EXISTS asignatura_curso;
DROP TABLE IF EXISTS asignatura_profesor;
DROP TABLE IF EXISTS asignatura_estudiante;
DROP TABLE IF EXISTS asignaturas;
DROP TABLE IF EXISTS curso;
DROP TABLE IF EXISTS colegio;
DROP TABLE IF EXISTS profesor;
DROP TABLE IF EXISTS estudiante;

CREATE TABLE colegio (
    id_colegio INT PRIMARY KEY,
    nombre_colegio VARCHAR(250) NOT NULL
);

CREATE TABLE curso (
    id_curso INT PRIMARY KEY,
    id_colegio INT NOT NULL,
    grado_curso INT NOT NULL,
    salon_curso VARCHAR(250) NOT NULL,
    CONSTRAINT colegio_id_fkey FOREIGN KEY (id_colegio) REFERENCES colegio(id_colegio)
);

CREATE TABLE estudiante (
    id_estudiante INT PRIMARY KEY,
    nombre_estudiante VARCHAR(250) NOT NULL
);

CREATE TABLE profesor (
    id_profesor INT PRIMARY KEY,
    nombre_profesor VARCHAR(250) NOT NULL
);

CREATE TABLE asignaturas (
    id_asignatura INT PRIMARY KEY,
    nombre_asignatura VARCHAR(250) NOT NULL
);

CREATE TABLE asignatura_estudiante (
    id_asginatura_estudiante INT PRIMARY KEY,
    id_asignatura INT NOT NULL,
    id_curso INT NOT NULL,
    id_estudiante INT NOT NULL,
    CONSTRAINT asignaturaestu_id_fkey FOREIGN KEY (id_asignatura) REFERENCES asignaturas(id_asignatura),
    CONSTRAINT asignaturacursoest_id_fkey FOREIGN KEY (id_curso) REFERENCES curso(id_curso),
    CONSTRAINT estudianteasig_id_fkey FOREIGN KEY (id_estudiante) REFERENCES estudiante(id_estudiante)
);

CREATE TABLE asignatura_profesor (
    id_asginatura_profesor INT PRIMARY KEY,
    id_asignatura INT NOT NULL,
    id_profesor INT NOT NULL,
    CONSTRAINT asignaturaprof_id_fkey FOREIGN KEY (id_asignatura) REFERENCES asignaturas(id_asignatura),
    CONSTRAINT profesorasig_id_fkey FOREIGN KEY (id_profesor) REFERENCES profesor(id_profesor)
);

CREATE TABLE asignatura_curso (
    id_asginatura_curso INT PRIMARY KEY,
    id_asignatura INT NOT NULL,
    id_curso INT NOT NULL,
    CONSTRAINT asignaturacurso_id_fkey FOREIGN KEY (id_asignatura) REFERENCES asignaturas(id_asignatura),
    CONSTRAINT cursoasig_id_fkey FOREIGN KEY (id_curso) REFERENCES curso(id_curso)
);

--Poblamiento de tablas
insert into colegio  (id_colegio, nombre_colegio) values (1, 'El colegio del Olimpo');

insert into curso  (id_curso, id_colegio, grado_curso, salon_curso) values (1, 1, 10, 'A');
insert into curso  (id_curso, id_colegio, grado_curso, salon_curso) values (2, 1, 10, 'B');
insert into curso  (id_curso, id_colegio, grado_curso, salon_curso) values (3, 1, 11, 'A');
insert into curso  (id_curso, id_colegio, grado_curso, salon_curso) values (4, 1, 11, 'B');

insert into estudiante  (id_estudiante, nombre_estudiante) values (1, 'Afrodita');
insert into estudiante  (id_estudiante, nombre_estudiante) values (2, 'Apolo');
insert into estudiante  (id_estudiante, nombre_estudiante) values (3, 'Ares');
insert into estudiante  (id_estudiante, nombre_estudiante) values (4, 'Artemisa');
insert into estudiante  (id_estudiante, nombre_estudiante) values (5, 'Atenea');
insert into estudiante  (id_estudiante, nombre_estudiante) values (6, 'Dionisio');
insert into estudiante  (id_estudiante, nombre_estudiante) values (7, 'Hefesto');
insert into estudiante  (id_estudiante, nombre_estudiante) values (8, 'Hera');
insert into estudiante  (id_estudiante, nombre_estudiante) values (9, 'Hermes');
insert into estudiante  (id_estudiante, nombre_estudiante) values (10, 'Hades');
insert into estudiante  (id_estudiante, nombre_estudiante) values (11, 'Poseidón');
insert into estudiante  (id_estudiante, nombre_estudiante) values (12, 'Zeus');

insert into profesor  (id_profesor, nombre_profesor) values (1, 'Némesis');
insert into profesor  (id_profesor, nombre_profesor) values (2, 'Príapo');
insert into profesor  (id_profesor, nombre_profesor) values (3, 'Iris');

insert into asignaturas  (id_asignatura, nombre_asignatura) values (1, 'Matemáticas');
insert into asignaturas  (id_asignatura, nombre_asignatura) values (2, 'Español');
insert into asignaturas  (id_asignatura, nombre_asignatura) values (3, 'Ingles básico');
insert into asignaturas  (id_asignatura, nombre_asignatura) values (4, 'Ingles avanzado');
insert into asignaturas  (id_asignatura, nombre_asignatura) values (5, 'Pre Icfes');

insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (1, 1, 1);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (2, 1, 2);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (3, 1, 3);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (4, 1, 4);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (5, 2, 1);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (6, 2, 2);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (7, 3, 1);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (8, 4, 2);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (9, 5, 3);
insert into asignatura_curso (id_asginatura_curso, id_asignatura, id_curso) values (10, 5, 4);

insert into asignatura_profesor (id_asginatura_profesor, id_asignatura, id_profesor) values (1, 1, 1);
insert into asignatura_profesor (id_asginatura_profesor, id_asignatura, id_profesor) values (2, 2, 2);
insert into asignatura_profesor (id_asginatura_profesor, id_asignatura, id_profesor) values (3, 3, 3);
insert into asignatura_profesor (id_asginatura_profesor, id_asignatura, id_profesor) values (4, 4, 3);
insert into asignatura_profesor (id_asginatura_profesor, id_asignatura, id_profesor) values (5, 5, 1);

insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (1, 1, 1, 1);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (2, 1, 1, 2);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (3, 1, 1, 3);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (4, 2, 1, 1);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (5, 2, 1, 2);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (6, 2, 1, 3);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (7, 3, 1, 1);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (8, 3, 1, 2);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (9, 3, 1, 3);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (10, 1, 2, 4);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (11, 1, 2, 5);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (12, 1, 2, 6);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (13, 2, 2, 4);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (14, 2, 2, 5);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (15, 2, 2, 6);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (16, 4, 2, 4);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (17, 4, 2, 5);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (18, 4, 2, 6);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (19, 1, 3, 7);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (20, 1, 3, 8);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (21, 5, 3, 7);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (22, 5, 3, 8);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (23, 1, 4, 9);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (24, 1, 4, 10);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (25, 1, 4, 11);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (26, 1, 4, 12);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (27, 5, 4, 9);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (28, 5, 4, 10);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (29, 5, 4, 11);
insert into asignatura_estudiante (id_asginatura_estudiante, id_asignatura, id_curso, id_estudiante) values (30, 5, 4, 12);