package com.test.test.controllers;

import com.test.test.services.ColegioService;
import com.test.test.services.ProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("colegio")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS})
public class ColegioController {

    @Autowired
    private ColegioService colegioService;

    @Autowired
    private ProfesorService profesorService;

    @GetMapping("test")
    public String test() {
        return "Test ColegioController 1.0.0";
    }

    /**
     * @param idColegio
     * @return
     */
    @GetMapping("consultarCursos/{idColegio}")
    public ResponseEntity consultarCursos(@PathVariable("idColegio") Integer idColegio) {
        return colegioService.consultarCursos(idColegio);
    }

    /**
     * @param idCurso
     * @return
     */
    @GetMapping("consultaDetalle/{idCurso}")
    public ResponseEntity consultaDetalle(@PathVariable("idCurso") Integer idCurso) {
        return colegioService.consultaDetalle(idCurso);
    }

    @GetMapping("consultarTodosProfesores")
    public ResponseEntity consultarTodosProfesores() {
        return profesorService.consultarTodosProfesores();
    }
    /**
     * @return
     */
    @GetMapping("consultarTodasAsignaturas")
    public ResponseEntity consultarTodasAsignaturas() {
        return colegioService.consultarTodasAsignaturas();
    }

}
