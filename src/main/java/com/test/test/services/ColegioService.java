package com.test.test.services;

import com.test.test.dto.*;
import com.test.test.entities.*;
import com.test.test.repositories.*;
import com.test.test.util.ConstanteComunes;
import com.test.test.util.MensajesComunes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ColegioService {

    @Autowired
    private ColegioRepository colegioRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private AsignaturaCursoRepositry asignaturaCursoRepositry;

    @Autowired
    private AsignaturaProfesorRepository asignaturaProfesorRepository;

    @Autowired
    private AsignaturaEstudianteRepository asignaturaEstudianteRepository;

    /**
     * @return
     */
    public ResponseEntity consultarTodasAsignaturas() {
        GenericoDTO salida = new GenericoDTO();
        salida.setCodigoResultado(ConstanteComunes.ESTADOS.FALLIDO.name());
        List<CursoEntity> cursoEntity = cursoRepository.findAll();
        if (!cursoEntity.isEmpty()) {
            List<DatosCursosOut> listaCursosOut = new ArrayList<>();
            for (CursoEntity c : cursoEntity) {
                DatosCursosOut datosCursosOut = new DatosCursosOut();
                datosCursosOut.setIdCurso(c.getIdCurso());
                datosCursosOut.setNombreCurso(c.getGradoCurso().toString() + c.getSalonCurso());
                List<DatosAsignaturaOut> listaDatosAsignaturaOut = new ArrayList<>();
                List<AsignaturaCursoEntity> listaAsignaturaCurso = asignaturaCursoRepositry.findByCursoEntity(c);
                if (!listaAsignaturaCurso.isEmpty()) {
                    asociarInformacionAsignatura(listaAsignaturaCurso, listaDatosAsignaturaOut);
                    datosCursosOut.setListaDatosAsignaturaOut(listaDatosAsignaturaOut);
                } else {
                    salida.setMensajeSalida(MensajesComunes.MSJ_ERROR_NO_DATOS_ASIGNATURA);
                }
                listaCursosOut.add(datosCursosOut);
            }
            salida.setCodigoResultado(ConstanteComunes.ESTADOS.EXITOSO.name());
            salida.setSalida(listaCursosOut);
        } else {
            salida.setMensajeSalida(MensajesComunes.MSJ_ERROR_NO_DATOS_CURSOS);
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(salida);
    }

    /**
     * @param idCurso
     * @return
     */
    public ResponseEntity consultaDetalle(Integer idCurso) {
        GenericoDTO salida = new GenericoDTO();
        salida.setCodigoResultado(ConstanteComunes.ESTADOS.FALLIDO.name());
        Optional<CursoEntity> cursoEntity = cursoRepository.findById(idCurso);
        if (cursoEntity.isPresent()) {
            DatosCursosOut datosCursosOut = new DatosCursosOut();
            datosCursosOut.setIdCurso(cursoEntity.get().getIdCurso());
            datosCursosOut.setNombreCurso(cursoEntity.get().getGradoCurso().toString() + cursoEntity.get().getSalonCurso());
            List<DatosAsignaturaOut> listaDatosAsignaturaOut = new ArrayList<>();
            List<AsignaturaCursoEntity> listaAsignaturaCurso = asignaturaCursoRepositry.findByCursoEntity(cursoEntity.get());
            if (!listaAsignaturaCurso.isEmpty()) {
                asociarInformacionAsignatura(listaAsignaturaCurso, listaDatosAsignaturaOut);
                datosCursosOut.setListaDatosAsignaturaOut(listaDatosAsignaturaOut);
                salida.setCodigoResultado(ConstanteComunes.ESTADOS.EXITOSO.name());
                salida.setSalida(datosCursosOut);
            } else {
                salida.setMensajeSalida(MensajesComunes.MSJ_ERROR_NO_DATOS_ASIGNATURA);
            }
        } else {
            salida.setMensajeSalida(MensajesComunes.MSJ_ERROR_NO_DATOS_CURSOS);
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(salida);
    }

    /**
     * @param listaAsignaturaCurso
     * @param listaDatosAsignaturaOut
     */
    private void asociarInformacionAsignatura(List<AsignaturaCursoEntity> listaAsignaturaCurso, List<DatosAsignaturaOut> listaDatosAsignaturaOut) {
        for (AsignaturaCursoEntity a : listaAsignaturaCurso) {
            DatosAsignaturaOut datosAsignaturaOut = new DatosAsignaturaOut();
            datosAsignaturaOut.setIdAsignatura(a.getAsignaturaEntity().getIdAsignatura());
            datosAsignaturaOut.setNombreAsignatura(a.getAsignaturaEntity().getNombreAsignatura());
            asociarProfesores(a.getAsignaturaEntity(), datosAsignaturaOut);
            listaDatosAsignaturaOut.add(datosAsignaturaOut);
        }
    }


    /**
     * @param asignaturaEntity
     */
    private void asociarProfesores(AsignaturaEntity asignaturaEntity, DatosAsignaturaOut datosAsignaturaOut) {
        Optional<AsignaturaProfesorEntity> asignaturaProfesor = asignaturaProfesorRepository.findByAsignaturaEntity(asignaturaEntity);
        if (asignaturaProfesor.isPresent()) {
            DatosProfesorOut datosProfesorOut = new DatosProfesorOut();
            datosProfesorOut.setIdProfesor(asignaturaProfesor.get().getProfesorEntity().getIdProfesor());
            datosProfesorOut.setNombreProfesor(asignaturaProfesor.get().getProfesorEntity().getNombreProfesor());
            datosAsignaturaOut.setDatosProfesor(datosProfesorOut);
        }
    }

    /**
     * @param idColegio
     * @return
     */
    public ResponseEntity consultarCursos(Integer idColegio) {
        GenericoDTO salida = new GenericoDTO();
        salida.setCodigoResultado(ConstanteComunes.ESTADOS.FALLIDO.name());
        Optional<ColegioEntity> colegioEntity = colegioRepository.findById(idColegio);
        if (colegioEntity.isPresent()) {
            DatosColegioOut datosColegioOut = new DatosColegioOut();
            datosColegioOut.setIdColegio(colegioEntity.get().getIdColegio());
            datosColegioOut.setNombreColegio(colegioEntity.get().getNombreColegio());
            List<CursoEntity> listCursos = cursoRepository.findByColegioEntity(colegioEntity.get());
            if (!listCursos.isEmpty()) {
                asociarCursos(listCursos, datosColegioOut);
                salida.setCodigoResultado(ConstanteComunes.ESTADOS.EXITOSO.name());
                salida.setSalida(datosColegioOut);
            } else {
                salida.setMensajeSalida(MensajesComunes.MSJ_ERROR_NO_DATOS_CURSOS);
            }
        } else {
            salida.setMensajeSalida(MensajesComunes.MSJ_ERROR_NO_DATOS_COLEGIO);
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(salida);
    }

    /**
     * @param listCursos
     * @param datosColegioOut
     */
    private void asociarCursos(List<CursoEntity> listCursos, DatosColegioOut datosColegioOut) {
        List<DatosCursosOut> listaCursosOut = new ArrayList<>();
        for (CursoEntity c : listCursos) {
            DatosCursosOut datosCursosOut = new DatosCursosOut();
            datosCursosOut.setIdCurso(c.getIdCurso());
            datosCursosOut.setNombreCurso(c.getGradoCurso().toString() + c.getSalonCurso());
            listaCursosOut.add(datosCursosOut);
        }
        datosColegioOut.setListaCursos(listaCursosOut);
    }

}
