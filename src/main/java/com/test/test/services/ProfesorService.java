package com.test.test.services;

import com.test.test.dto.*;
import com.test.test.entities.*;
import com.test.test.repositories.AsignaturaCursoRepositry;
import com.test.test.repositories.AsignaturaEstudianteRepository;
import com.test.test.repositories.AsignaturaProfesorRepository;
import com.test.test.repositories.ProfesorRepository;
import com.test.test.util.ConstanteComunes;
import com.test.test.util.MensajesComunes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProfesorService {

    @Autowired
    private AsignaturaProfesorRepository asignaturaProfesorRepository;

    @Autowired
    private AsignaturaCursoRepositry asignaturaCursoRepositry;

    @Autowired
    private AsignaturaEstudianteRepository asignaturaEstudianteRepository;

    @Autowired
    private ProfesorRepository profesorRepository;

    /**
     * @return
     */
    public ResponseEntity consultarTodosProfesores() {
        GenericoDTO salida = new GenericoDTO();
        salida.setCodigoResultado(ConstanteComunes.ESTADOS.FALLIDO.name());
        List<ProfesorEntity> listaProfesores = profesorRepository.findAll();
        if (!listaProfesores.isEmpty()) {
            List<DatosProfesorOut> listaProfesoresOut = new ArrayList<>();
            for (ProfesorEntity p : listaProfesores) {
                DatosProfesorOut datosProfesorOut = new DatosProfesorOut();
                datosProfesorOut.setIdProfesor(p.getIdProfesor());
                datosProfesorOut.setNombreProfesor(p.getNombreProfesor());
                List<AsignaturaProfesorEntity> listAsignaturaProfesor = asignaturaProfesorRepository.findByProfesorEntity(p);
                if (!listAsignaturaProfesor.isEmpty()) {
                    asociarAsginatura(listAsignaturaProfesor, datosProfesorOut);
                }
                listaProfesoresOut.add(datosProfesorOut);
            }
            salida.setCodigoResultado(ConstanteComunes.ESTADOS.EXITOSO.name());
            salida.setSalida(listaProfesoresOut);
        } else {
            salida.setMensajeSalida(MensajesComunes.MSJ_ERROR_NO_DATOS_PROFESORES);
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(salida);
    }

    /**
     * @param listAsignaturaProfesor
     * @param datosProfesorOut
     */
    private void asociarAsginatura(List<AsignaturaProfesorEntity> listAsignaturaProfesor, DatosProfesorOut datosProfesorOut) {
        List<DatosAsignaturaOut> listaAsignaturasOut = new ArrayList<>();
        for (AsignaturaProfesorEntity a : listAsignaturaProfesor) {
            DatosAsignaturaOut datosAsignaturaOut = new DatosAsignaturaOut();
            datosAsignaturaOut.setIdAsignatura(a.getAsignaturaEntity().getIdAsignatura());
            datosAsignaturaOut.setNombreAsignatura(a.getAsignaturaEntity().getNombreAsignatura());
            List<AsignaturaCursoEntity> listaCursoEntity = asignaturaCursoRepositry.findByAsignaturaEntity(a.getAsignaturaEntity());
            if (!listaCursoEntity.isEmpty()) {
                List<DatosCursosOut> listCursosOut = new ArrayList<>();
                for (AsignaturaCursoEntity c : listaCursoEntity) {
                    DatosCursosOut datosCursosOut = new DatosCursosOut();
                    datosCursosOut.setIdCurso(c.getCursoEntity().getIdCurso());
                    datosCursosOut.setNombreCurso(c.getCursoEntity().getGradoCurso().toString() + c.getCursoEntity().getSalonCurso());
                    asociarEstudiantes(a.getAsignaturaEntity(), c.getCursoEntity(), datosCursosOut);
                    listCursosOut.add(datosCursosOut);
                }
                datosAsignaturaOut.setDatosCursosOut(listCursosOut);
            }
            listaAsignaturasOut.add(datosAsignaturaOut);
        }
        datosProfesorOut.setListaAsignaturas(listaAsignaturasOut);
    }

    /**
     * @param asignaturaEntity
     * @param datosCursosOut
     */
    private void asociarEstudiantes(AsignaturaEntity asignaturaEntity, CursoEntity cursoEntity, DatosCursosOut datosCursosOut) {
        List<AsignaturaEstudianteEntity> listaAsignaturaEstudiante = asignaturaEstudianteRepository.findByAsignaturaEntityAndCursoEntity(asignaturaEntity, cursoEntity);
        if (!listaAsignaturaEstudiante.isEmpty()) {
            List<DatosEstudianteOut> listaEstudiantesOut = new ArrayList<>();
            for (AsignaturaEstudianteEntity a : listaAsignaturaEstudiante) {
                DatosEstudianteOut datosEstudianteOut = new DatosEstudianteOut();
                datosEstudianteOut.setIdEstudiente(a.getEstudianteEntity().getIdEstudiante());
                datosEstudianteOut.setNombreEstudiante(a.getEstudianteEntity().getNombreEstudiante());
                listaEstudiantesOut.add(datosEstudianteOut);
            }
            datosCursosOut.setListaEstudiante(listaEstudiantesOut);
        }
    }

}
