package com.test.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "colegio")
@Cacheable(false)
@Data
@NoArgsConstructor
public class ColegioEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_colegio")
    private Integer idColegio;
    @Column(name = "nombre_colegio")
    private String nombreColegio;

}
