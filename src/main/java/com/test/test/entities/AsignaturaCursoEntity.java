package com.test.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "asignatura_curso")
@Cacheable(false)
@Data
@NoArgsConstructor
public class AsignaturaCursoEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_asginatura_curso")
    private Integer idAsignaturaCurso;
    @JoinColumn(name = "id_asignatura")
    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
    private AsignaturaEntity asignaturaEntity;
    @JoinColumn(name = "id_curso")
    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
    private CursoEntity cursoEntity;

}
