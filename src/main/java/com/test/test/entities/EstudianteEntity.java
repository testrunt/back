package com.test.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "estudiante")
@Cacheable(false)
@Data
@NoArgsConstructor
public class EstudianteEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_estudiante")
    private Integer idEstudiante;
    @Column(name = "nombre_estudiante")
    private String nombreEstudiante;

}
