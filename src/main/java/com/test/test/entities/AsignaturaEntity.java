package com.test.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "asignaturas")
@Cacheable(false)
@Data
@NoArgsConstructor
public class AsignaturaEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_asignatura")
    private Integer idAsignatura;
    @Column(name = "nombre_asignatura")
    private String nombreAsignatura;

}
