package com.test.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "asignatura_profesor")
@Cacheable(false)
@Data
@NoArgsConstructor
public class AsignaturaProfesorEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_asginatura_profesor")
    private Integer idAsignaturaProfesor;
    @JoinColumn(name = "id_asignatura")
    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
    private AsignaturaEntity asignaturaEntity;
    @JoinColumn(name = "id_profesor")
    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
    private ProfesorEntity profesorEntity;

}
