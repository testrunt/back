package com.test.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "curso")
@Cacheable(false)
@Data
@NoArgsConstructor
public class CursoEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_curso")
    private Integer idCurso;
    @JoinColumn(name = "id_colegio")
    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
    private ColegioEntity colegioEntity;
    @Column(name = "grado_curso")
    private Integer gradoCurso;
    @Column(name = "salon_curso")
    private String salonCurso;

}
