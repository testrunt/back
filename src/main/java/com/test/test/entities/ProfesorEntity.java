package com.test.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "profesor")
@Cacheable(false)
@Data
@NoArgsConstructor
public class ProfesorEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_profesor")
    private Integer idProfesor;
    @Column(name = "nombre_profesor")
    private String nombreProfesor;

}
