package com.test.test.repositories;

import com.test.test.entities.AsignaturaCursoEntity;
import com.test.test.entities.AsignaturaEntity;
import com.test.test.entities.CursoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AsignaturaCursoRepositry extends JpaRepository<AsignaturaCursoEntity, Integer> {

    /**
     * @param cursoEntity
     * @return
     */
    public List<AsignaturaCursoEntity> findByCursoEntity(CursoEntity cursoEntity);

    /**
     * @param asignaturaEntity
     * @return
     */
    public List<AsignaturaCursoEntity> findByAsignaturaEntity(AsignaturaEntity asignaturaEntity);

}
