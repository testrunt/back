package com.test.test.repositories;

import com.test.test.entities.AsignaturaEntity;
import com.test.test.entities.AsignaturaProfesorEntity;
import com.test.test.entities.ProfesorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AsignaturaProfesorRepository extends JpaRepository<AsignaturaProfesorEntity, Integer> {

    /**
     * @param asignaturaEntity
     * @return
     */
    public Optional<AsignaturaProfesorEntity> findByAsignaturaEntity(AsignaturaEntity asignaturaEntity);

    /**
     * @param profesorEntity
     * @return
     */
    public List<AsignaturaProfesorEntity> findByProfesorEntity(ProfesorEntity profesorEntity);

}
