package com.test.test.repositories;

import com.test.test.entities.ColegioEntity;
import com.test.test.entities.CursoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CursoRepository extends JpaRepository<CursoEntity, Integer> {

    /**
     * @param colegioEntity
     * @return
     */
    public List<CursoEntity> findByColegioEntity(ColegioEntity colegioEntity);

}
