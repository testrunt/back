package com.test.test.repositories;

import com.test.test.entities.ColegioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ColegioRepository extends JpaRepository<ColegioEntity, Integer> {

}
