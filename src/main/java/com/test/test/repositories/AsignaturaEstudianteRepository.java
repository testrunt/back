package com.test.test.repositories;

import com.test.test.entities.AsignaturaEntity;
import com.test.test.entities.AsignaturaEstudianteEntity;
import com.test.test.entities.CursoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AsignaturaEstudianteRepository extends JpaRepository<AsignaturaEstudianteEntity, Integer> {

    /**
     * @param asignaturaEntity
     * @param cursoEntity
     * @return
     */
    public List<AsignaturaEstudianteEntity> findByAsignaturaEntityAndCursoEntity(AsignaturaEntity asignaturaEntity, CursoEntity cursoEntity);

}
