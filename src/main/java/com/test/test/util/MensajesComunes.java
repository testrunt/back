package com.test.test.util;

public class MensajesComunes {

    /**
     *
     */
    public static final String MSJ_ERROR_NO_DATOS_COLEGIO = "MSJ_ERROR_NO_DATOS_COLEGIO";

    /**
     *
     */
    public static final String MSJ_ERROR_NO_DATOS_CURSOS = "MSJ_ERROR_NO_DATOS_CURSOS";

    /**
     *
     */
    public static final String MSJ_ERROR_NO_DATOS_ASIGNATURA = "MSJ_ERROR_NO_DATOS_ASIGNATURA";

    /**
     *
     */
    public static final String MSJ_ERROR_NO_DATOS_PROFESORES = "MSJ_ERROR_NO_DATOS_PROFESORES";

}
