package com.test.test.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DatosCursosOut {

    private Integer idCurso;
    private String nombreCurso;
    private List<DatosAsignaturaOut> listaDatosAsignaturaOut;
    private List<DatosProfesorOut> listaProfesores;
    private List<DatosEstudianteOut> listaEstudiante;

}
