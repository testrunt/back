package com.test.test.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DatosAsignaturaOut {

    private Integer idAsignatura;
    private String nombreAsignatura;
    private DatosProfesorOut datosProfesor;
    private List<DatosCursosOut> datosCursosOut;
    private List<DatosEstudianteOut> listaEstudiante;

}
