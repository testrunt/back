package com.test.test.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenericoDTO {

    private String codigoResultado;
    private String mensajeSalida;
    private Object salida;

}
