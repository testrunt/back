package com.test.test.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DatosColegioOut {

    private Integer idColegio;
    private String nombreColegio;
    private List<DatosCursosOut> listaCursos;

}
