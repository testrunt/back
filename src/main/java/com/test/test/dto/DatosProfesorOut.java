package com.test.test.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DatosProfesorOut {

    private Integer idProfesor;
    private String nombreProfesor;
    private List<DatosAsignaturaOut> listaAsignaturas;

}
