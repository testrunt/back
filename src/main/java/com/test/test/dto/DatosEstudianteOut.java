package com.test.test.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DatosEstudianteOut {

    private Integer idEstudiente;
    private String nombreEstudiante;

}
